/**
 * Created by Dennis on 3/20/15.
 */

var app = angular.module('app', ['ui.router']);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/page1');

    // Define routes
    $stateProvider
        .state('main',{
            templateUrl: 'templates/main.tmpl.html',
            abstract: true
        })

        // Main pages
        .state('page1',{
            parent: 'main',
            url: '/page1',
            templateUrl: 'templates/page1.tmpl.html',
            controller: function($scope){

            }
        })
        .state('page2',{
            parent: 'main',
            url: '/page2',
            templateUrl: 'templates/page2.tmpl.html',
            controller: function($scope){

            }
        })
        .state('page3',{
            parent: 'main',
            url: '/page3',
            templateUrl: 'templates/page3.tmpl.html',
            controller: function($scope){

            }
        })
        .state('page4',{
            parent: 'main',
            url: '/page4',
            templateUrl: 'templates/page4.tmpl.html',
            controller: function($scope){

            }
        });
}]);